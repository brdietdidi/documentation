With this project, I like to show a way to do (SW-) project documentation following the _doc-as-code_ approach (see [Documentation As Code](https://docs-as-co.de/) and [Write The Docs: Doc as Code](https://www.writethedocs.org/guide/docs-as-code/)).

The project uses the [doctoolchain](https://doctoolchain.org/docToolchain/v2.0.x/index.html) to generate the documentation artefacts.

As recommended by the used toolset, the documentation is written in plain ASCII and the [asciidoc format](https://asciidoc.org/).

You can find all the docs within the `src/docs` folder.

The CI-Build publishes this documentation automatically to

- [Confluence](https://brdietdidi.atlassian.net/wiki/spaces/documentat/overview) and
- as Site to [Gitlab Pages](https://documentation.dieterbaier.eu)

Check the [.gitlab-ci.yml](.gitlab-ci.yml) as reference.

So, if you are interested in the following topics

- How to use the docToolchain?
- How to do an architectural documentation with arc42?
- How to publish my documentation written in ASCII and managed by Git to Confluence?
- How to serve my documentation as a nice site?
- How to deploy my site on Gitlab-Pages?
- ...

you could find references within this project.

## Setup the docToolchain

### Install the Wrapper
```shell
curl -Lo dtcw https://doctoolchain.org/dtcw
chmod +x dtcw
```
If you are on MS Windows, you can get `dtcw.bat` or `dtcw.ps1`. Just replace `dtcw` with the corresponding file you like to download

### (optional) Install the docToolchain locally
In case, you can't or don't want to use `docker`, you can just install the docToolchain locally. If you'll use docker for using the docToolchain, you can skip this step.

> NOTE
> 
> If you are going to use a local docToolchain installation, you need to have java >= 11 <= 17 installed
> 
```shell
./dtcw local install doctoolchain
```

> NOTE
> 
> When you use a local docToolchain installation, the first execution of the wrapper will take some time, since it will download some dependencies

### Create your `docs` folder
```shell
mkdir src/docs
```

### Download the arc42 template
```shell
./dtcw downloadTemplate 
```
- Let the script create the docToolchain configuration file `docToolchainConfig.groovy`
- select to download the arc42 template (you could also install the req42 template)
- select the language you like for the doc templates
- select, if you like to have the arc42 help texts within the doc templates
- for now, I didn't experiment with the docToolchain in combination with Antora

You will find the arc42 template files within the `src/docs` folder.

### Generate a pure HTML output of your documentation
```shell
./dtcw generateHTML
```

This will generate a single page of the arc42 documentation below `build/html5/arc42/modules/ROOT/pages`. There you'll find a `index.html` which you can simply access with your browser.

> NOTE
> 
> If you plan to publish your documentation to Confluence, the docToolchain will generate exactly this output before it is going to publish the result to Confluence. Be stay cool: it will not create a single page at Confluence. It will look cool there. Check the chaptor below.

### Generate a nice Site of your documentation
If you selected _Antora_ to set up your templates during the template download, you can generate a nice Site of your documentation which you could deploy to any webserver which can serve static websites.
```shell
./dtcw generateSite
```
> INFO
> 
> You might face some outputs saying _BUILD FAILED_. Don't scare about that.
> 
> For any reason there is a link `src/docs/arc42/modules/ROOT/examples/_includes` which points to `build/_includes` after setting up the template. But below the build folder, you'll not find an _includes folder.
> 
> Don't know, for what this link is good for. So I just deleted the link and everything worked fine.

The site is generated below `build/microsite/output`. This, you can deploy to any webserver or just open the `index.html` within a browser. If you open the site without having it deployed, there might be some failures on the site, since javascript will not work in this case.

#### Configuration adjustments to have the site configured _correctly_
You might face some stuff generated within your site, which should not be generated according to the arc42-template.

The arc42-template might use the `optimize-content` asciidoc attribute to render some content only in some cases (check the [index.adoc](src/docs/arc42/modules/ROOT/pages/index.adoc)) for example.

Since the `generateSite` task uses `jBake` for rendering the site, you need to set the asciidoc attribute for jBake. To do so, add the following configuration into the [docToolchainConfig.groovy](docToolchainConfig.groovy) file:

```groovy
jbake.with {
    asciidoctorAttributes = ["optimize-content='true'"]
}
```

## Publish docs to Confluence

Check the corresponding [Blog Post](src/docs/Blog/toolchain/BLOG_publishDocToConfluence.adoc) for getting info about this feature.