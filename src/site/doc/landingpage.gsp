<div class="row flex-xl-nowrap">
    <main class="col-12 col-md-12 col-xl-12 pl-md-12" role="main">
        <div class="bg-light p-5 rounded">
            <h1>All About Documentation</h1>
            <p class="lead">
                With the help of docToolchain, asciidoc, doc-as-code and friends
            </p>
            <p>Welcome to this page</p>
            <p>However you got to this page, I'm delighted.<br/>
                It's actually an artefact of my documentation sandbox, where I deal with doc-as-code, docToolchain, arc42, asciidoc etc.<br/>
            This page was generated with the docToolchain.<br/>
            Although it is actually a sandbox, useful information can certainly be found here. But probably more interesting is the Git repository, where the sources for the contents of this page can be found.
            </p>
        </div>

        <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
            <div class="col">
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 fw-normal">Blog</h4>
                    </div>
                    <div class="card-body">
                        Actually, I just want to try out how a blog can be managed with the toolchain that has been set up. But I'm sure you'll also find some useful information about documentation and my toolchain.
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 fw-normal">arc42</h4>
                    </div>
                    <div class="card-body">
                        With my daily work as a software developer, I naturally also deal with documentation and accordingly with architecture documentation. arc42 has been a long-time companion and I want to share a few tips and tricks here.
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 fw-normal">Toolchain</h4>
                    </div>
                    <div class="card-body">
                        On this page, or rather with the sources of this page, I want to show examples of different documentation requirements and how they can be implemented with my toolchain.
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cols-1 ro-cols-md-3 mb-3 text-center">
            <div class="col">
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 fw-normal">Publish to Confluence</h4>
                    </div>
                    <div class="card-body">
                        Learn how to use the docToolchain to deploy your ASCII documentation to Confluence
                        <br/>
                        <br/>
                        <a class="btn btn-primary" href="Blog/toolchain/BLOG_publishDocToConfluence.html" role="button">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </main>

</div>
